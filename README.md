# Kotlin Concurrency "Experiments"

## Purpose
Experimenting with Kotlin concurrency using the classic pi-by-monte-carlo problem.  May include others, may include gui(tornadofx?) at a later time.

## Prereq's
Java 1.8
May need Kotlin plugin locally

## Noteworthy Commands:
`./gradlew run` compiles and runs the project
