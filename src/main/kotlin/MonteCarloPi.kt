import kotlinx.coroutines.experimental.*
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicLong
import java.util.stream.LongStream
import kotlin.system.measureTimeMillis

fun testRandomPoint(): Boolean {
    val x = ThreadLocalRandom.current().nextDouble()
    val y = ThreadLocalRandom.current().nextDouble()
    return x * x + y * y <= 1.0
}

suspend fun testRandomPointAsync(): Boolean {
    return testRandomPoint()
}

fun warmUp(warmUpTime: Long) {
    val startTime  = Date().getTime()
    while (Date().getTime() - startTime < warmUpTime) {
        testRandomPoint()
    }
}

fun runSequential(sampleCount: Long):Double {
    var insideCount = 0L
    for(i in 1..sampleCount) {
        //if(ThreadLocalRandom.current().nextInt(10_000_000) < 2)
          //  println("Thread Count: " + java.lang.Thread.activeCount())
        if(testRandomPoint()) insideCount++
    }
    return (4.0 * insideCount) / sampleCount
}

suspend fun runAsyncNoAtomic(sampleCount: Long):Double {
    var insideCount = 0L

    async {
        for (i in 1..sampleCount) {
            if(testRandomPointAsync()) insideCount++
        }
    }.await()
    return (4.0 * insideCount) / sampleCount
}

suspend fun runAsyncWithAtomic(sampleCount: Long):Double {
    var insideCount = AtomicLong(0L)

    async {
        for (i in 1..sampleCount) {
            if(testRandomPointAsync()) insideCount.addAndGet(1)
        }
    }.await()
    return (4.0 * insideCount.get()) / sampleCount
}

fun runStreaming(sampleCount: Long):Double {
    var insideCount = AtomicLong(0)
    LongStream.rangeClosed(1,sampleCount).parallel().forEach {
        //if(ThreadLocalRandom.current().nextInt(10_000_000) < 2)
          //  println("Thread Count: " + java.lang.Thread.activeCount())
        if(testRandomPoint()){
            insideCount.getAndIncrement()
        }
    }
    return(4.0 * insideCount.get()) / sampleCount
}

fun runAsync3(sampleCount: Long): Double {
    var insideCount = AtomicLong(0)
    for(i in 1..sampleCount){
        async {
            if(testRandomPoint()){
                insideCount.getAndIncrement()
            }
        }
    }
    return (4.0 * insideCount.get())/sampleCount
}

fun main(args:Array<String>){
    println("start")
    val sampleCount = 50_000_000L
    val warmUpSeconds = 10L
    val warmUpTime = warmUpSeconds * 1000
    println("warming up for $warmUpSeconds seconds")
    warmUp(warmUpTime)
    println("Starting Sequential\nSequential:")
    println("\tTime in millis: " + measureTimeMillis { println(runSequential(sampleCount)) })
    //println("Starting Async with no atomics\nAsync no atomics:")
    //println("\tTime in millis: " +measureTimeMillis { println(runBlocking{runAsyncNoAtomic(sampleCount)}) })
    //println("Starting Async with atomics\nAsync atomics:")
    //println("\tTime in millis: " +measureTimeMillis { println(runBlocking{runAsyncWithAtomic(sampleCount)}) })
    println("Starting Streams\nStreams")
    println("\tTime in millis: " + measureTimeMillis { println(runBlocking { runStreaming(sampleCount) }) })
    println("Starting async3\n Asyn3")
    println("\tTime in millis: " + measureTimeMillis { println(runBlocking { runAsync3(sampleCount) }) })
    println("done")
}
